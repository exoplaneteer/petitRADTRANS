{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Retrievals: Dealing with multiple datasets\n",
    "### An advanced retrieval tutorial.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Written by [Evert Nasedkin](mailto:nasedkinevert@gmail.com?subject=[petitRADTRANS]%20Retrievals) and \n",
    "Paul Mollière\n",
    "\n",
    "[Based on Mollière (2020)](https://arxiv.org/abs/2006.09394)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial will use JWST observations of WASP39 b to demonstrate how to incorporate multiple datasets into the pRT retrieval framework.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting started\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should already have an installation of pRT on your machine, please see the [installation manual](../installation.html) if you have not installed it yet. \n",
    "[pyMultiNest](https://github.com/JohannesBuchner/PyMultiNest) is also required. [Ultranest](https://johannesbuchner.github.io/UltraNest/index.html) is required if you want to use that as your sampler rather than pyMultiNest. See the Ultranest documentation for why you might want to choose this method.\n",
    "Using nested sampling rather than MCMC is faster, handles multimodal cases better, and directly provides the Bayesian evidence, which allows for easier model comparison. \n",
    "\n",
    "\n",
    "In this tutorial, we will outline the process of setting up a RetrievalConfig object, which is the class used to set up a pRT retrieval.\n",
    "The basic process will always be to set up the configuration, and then pass it to the Retrieval class to run the retrieval using pyMultiNest.\n",
    "Several standard plotting outputs will also be produced by the retrieval class.\n",
    "Most of the classes and functions used in this tutorial have more advanced features than what will be explained here, so it's highly recommended to take a look at the code and API documentation. \n",
    "There should be enough flexibility built in to cover most typical retrieval studies, but if you have feature requests please get in touch, or open an issue on [gitlab](https://gitlab.com/mauricemolli/petitRADTRANS.git).\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's start by importing everything we need.\n",
    "import os\n",
    "# To not have numpy start parallelizing on its own\n",
    "os.environ[\"OMP_NUM_THREADS\"] = \"1\"\n",
    "#os.environ[\"pRT_input_data_path\"] = \"/path/to/petitRADTRANS/petitRADTRANS/input_data/\"\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from petitRADTRANS import Radtrans\n",
    "from petitRADTRANS import nat_cst as nc\n",
    "\n",
    "# Import the class used to set up the retrieval.\n",
    "from petitRADTRANS.retrieval import Retrieval,RetrievalConfig\n",
    "\n",
    "# Import Prior functions, if necessary.\n",
    "from petitRADTRANS.retrieval.util import gaussian_prior\n",
    "\n",
    "# Import atmospheric model function\n",
    "from petitRADTRANS.retrieval.models import guillot_patchy_transmission"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets start out by setting up a simple run definition.\n",
    "We'll add the data AFTER we define the model function below\n",
    "Full details of the parameters can be found in the [API documentation](../../autoapi/petitRADTRANS/retrieval/retrieval_config/index.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Lets start out by setting up a simple run definition\n",
    "# Full details of the parameters can be found in retrieval_config.py\n",
    "\n",
    "# Since our retrieval has already ran before, we'll set the mode to 'evaluate' so we can make some plots.\n",
    "RunDefinition = RetrievalConfig(retrieval_name = \"WASP39b_Guillot_FreeChem_PatchyGreyHaze\", \n",
    "                                      run_mode = \"retrieval\", # This must be 'retrieval' to run PyMultiNest    \n",
    "                                      pressures = np.logspace(-8,3,100), # Extend up to 10^-8 bar    \n",
    "                                      AMR = False, # We won't be using adaptive mesh refinement for the pressure grid        \n",
    "                                      scattering = False) # This would turn on scattering when calculating emission spectra.\n",
    "                                                          # Scattering is automatically included for transmission spectra.\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data\n",
    "Let's start with reading in the data. The data must be a 1D spectrum with error bars or a covariance matrix.\n",
    "\n",
    "As in the basic tutorial, we're reading in text files, but this time we also include the column that describes the wavelength bins:\n",
    "```\n",
    "# Wavelength [micron], Bins [micron], Flux [W/m2/micron or (Rp/Rstar)^2], Flux Error [W/m2/micron or (Rp/Rstar)^2]\n",
    "```\n",
    "\n",
    "The Data class is arguably the most important part of setting up the retrieval. \n",
    "Not only do you input your data here, but you also choose your model function and resolution. \n",
    "This means that you can design a retrieval around different datatypes and retrieve simultaneously on both - for example, if you want the day and nightside of a planet, or want to combine the eastward and westward limbs of a transmission spectrum with different models. \n",
    "\n",
    "You can also set a distance to your object, which will allow you to automatically scale the flux and error of your data using the `scale_to_distance()` method - useful if you have data normalized to 10pc! Finally, there's also a arguments `scale`, `scale_err` and `offset_bool`, which tells the retrieval that the flux or uncertaines should be scaled by an arbitrary multiplicative factor or have an additive offset, both which is set up as a normal retrieval parameter using the `RetrivalConfig.add_parameter()` method. \n",
    "The name must be of the format DATANAME_scale_factor or DATANAME_offset. \n",
    "This is useful if two datasets are incompatible in absolute photometry, but you still want to use the spectral shape to inform the retrieval.\n",
    "\n",
    "In this retrieval we're going to include several datasets from different JWST instruments, starting with NIRISS SOSS orders 1 and 2. \n",
    "To include both of them , we simply add more than one dataset to our `RetrievalConfig` object. \n",
    "Notice that we're also telling the retrieval that we want the order 2 data to have an additive offset: this will allow the data to float relative to order one, which remains fixed. This can be used to compensate for differences in transit depth between different instruments.\n",
    "\n",
    "We're also using the buillt in `guillot_patchy_transmission` model for this retrieval, rather than writing our own model function from scratch.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import petitRADTRANS # need to get the name for the example data\n",
    "path_to_data = \"\" \n",
    "path_to_data = petitRADTRANS.__file__.split('__init__.py')[0] # Default location for the example\n",
    "transmission_directory = \"retrieval/examples/transmission/\"\n",
    "RunDefinition.add_data('JWST/NIRISSSOSS/O1',\n",
    "                       f\"{path_to_data}{transmission_directory}observations/JWST/WASP39b_niriss_soss1.txt\",\n",
    "                       data_resolution=700,\n",
    "                       model_resolution=300,\n",
    "                       model_generating_function = guillot_patchy_transmission,\n",
    "                       external_pRT_reference=None)\n",
    "RunDefinition.add_data('JWST/NIRISSSOSS/O2',\n",
    "                       f\"{path_to_data}{transmission_directory}observations/JWST/WASP39b_niriss_soss2.txt\",\n",
    "                       data_resolution=700,\n",
    "                       model_resolution=300,\n",
    "                       offset_bool=True,\n",
    "                       model_generating_function = guillot_patchy_transmission,\n",
    "                       external_pRT_reference = None)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## External references\n",
    "Sometimes the datasets will include regions that overlap in wavelength, or where one dataset falls entirely within the wavelength range of another dataset.\n",
    "In that case we can use an external reference: the dataset that falls within the wavelength range of the other will use the spectrum computed using the other dataset's `Radtrans` to compute the likelihood. \n",
    "This way we only need to compute the spectrum once in the same wavelength range, rather than initialising two `Radtrans` and calculating a spectrum for each.\n",
    "However, be careful here: the model resolution in the reference object should be high enough to properly sample any datasets that reference it!\n",
    "\n",
    "In this example, the NIRSpec PRISM data covers the entire NIRISS SOSS wavelength range, so we can use it as a reference for both NIRISS SOSS orders."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "RunDefinition.data = {} # Remove the previous data that was added above.\n",
    "RunDefinition.add_data('JWST/NIRSPEC/PRISM',\n",
    "                       f\"{path_to_data}{transmission_directory}observations/JWST/WASP39b_nirspec_prism.txt\",\n",
    "                       data_resolution=100,\n",
    "                       model_resolution=300,\n",
    "                       offset_bool=True,\n",
    "                       model_generating_function = guillot_patchy_transmission)\n",
    "RunDefinition.add_data('JWST/NIRISSSOSS/O1',\n",
    "                       f\"{path_to_data}{transmission_directory}observations/JWST/WASP39b_niriss_soss1.txt\",\n",
    "                       data_resolution=700,\n",
    "                       model_resolution=300,\n",
    "                       model_generating_function = guillot_patchy_transmission,\n",
    "                       external_pRT_reference='JWST/NIRSPEC/PRISM')\n",
    "RunDefinition.add_data('JWST/NIRISSSOSS/O2',\n",
    "                       f\"{path_to_data}{transmission_directory}observations/JWST/WASP39b_niriss_soss2.txt\",\n",
    "                       data_resolution=700,\n",
    "                       model_resolution=300,\n",
    "                       offset_bool=True,\n",
    "                       model_generating_function = guillot_patchy_transmission,\n",
    "                       external_pRT_reference = 'JWST/NIRSPEC/PRISM')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Model parameters\n",
    "Here we're using a more complicated atmospheric model to fit the JWST data.\n",
    "The temperature profile is taken from Guillot 2010, and includes four parameters to describe the shape.\n",
    "We're freely retrieving the chemical abundances, and include both patchy grey clouds and an enhanced power law slope as a proxy for hazes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# WASP 39 parameters\n",
    "RunDefinition.add_parameter(name = 'D_pl', free = False, value = 230.0*nc.pc)\n",
    "RunDefinition.add_parameter(name = 'Rstar', free = False, value = 0.9324 * nc.r_sun)\n",
    "\n",
    "# Fix the reference pressure in bar\n",
    "RunDefinition.add_parameter('reference_pressure',False,value  = 0.01)\n",
    "\n",
    "\n",
    "# Choose two of log_g, radius and mass priors\n",
    "RunDefinition.add_parameter('log_g',True,\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 2.0+3.5*x)\n",
    "RunDefinition.add_parameter('R_pl', True,\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 0.8*nc.r_jup_mean+ (x*0.8*nc.r_jup_mean))\n",
    "\n",
    "# Priors for Guillot 2010 Temperature Profile\n",
    "RunDefinition.add_parameter(\"T_int\", True,\\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 100 + 3500*x)\n",
    "RunDefinition.add_parameter(\"T_equ\", True,\\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : 100 + 3500*x)\n",
    "RunDefinition.add_parameter(\"gamma\", True,\\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x :  10**(-(x/2.)**2./2.))\n",
    "RunDefinition.add_parameter(\"log_kappa_IR\", True,\\\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : -4.0 + 6.0*x)\n",
    "\n",
    "# Grey cloud top pressure\n",
    "RunDefinition.add_parameter('log_Pcloud',True,\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : -8.+11.*x)\n",
    "\n",
    "# Enhanced haze scattering slope                            \n",
    "RunDefinition.add_parameter('kappa_0',True,\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : -4.+14.*x)\n",
    "RunDefinition.add_parameter('gamma_scat',True,\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : -20.+22.*x)\n",
    "# Cloud fraction                            \n",
    "RunDefinition.add_parameter('patchiness',True,\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : x)\n",
    "\n",
    "# Data offsets\n",
    "RunDefinition.add_parameter('JWST/NIRSPEC/PRISM_offset',True,\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : gaussian_prior(x, 0, 1e-4))\n",
    "RunDefinition.add_parameter('JWST/NIRISSSOSS/O2_offset',True,\n",
    "                            transform_prior_cube_coordinate = \\\n",
    "                            lambda x : gaussian_prior(x, 0, 1e-4))\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Opacities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "RunDefinition.set_rayleigh_species(['H2', 'He'])\n",
    "RunDefinition.set_continuum_opacities(['H2-H2', 'H2-He'])\n",
    "\n",
    "# Here we setup the line species for a free retrieval, setting the prior bounds with the abund_lim parameter\n",
    "# The retrieved value is the log mass fraction.\n",
    "# Let's use the most up-to-date line lists\n",
    "RunDefinition.set_line_species([\"H2O_Exomol\", \"CO_all_iso_HITEMP\", \"CO2\", \"CH4\"], eq=False, abund_lim = (-8.0,0.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's set up some plotting details so that we can generate nice outputs.\n",
    "\n",
    "Each parameter can be added to the corner plot, its label changed, and the values transformed to more digestible units (e.g., the planet radius in jupiter radii, rather than cm).\n",
    "We can also set the bounds and scaling on the best-fit spectrum plot and the limits for the P-T profile plot.\n",
    "With this complete, our retrieval is ready to go.\n",
    "\n",
    "Most parameters include a default setting, so the plots will be created even if you don't set any plotting parameters, but the outputs might not be very informative. In general, the possible arguments to `plot_kwargs` follow the naming conventions of matplotlib arguments and functions, with some additions.\n",
    "Full details of the plotting can be found in the Retrieval class, see the [API documentation](../../autoapi/petitRADTRANS/retrieval/retrieval/index.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "##################################################################\n",
    "# Define what to put into corner plot if run_mode == 'evaluate'\n",
    "##################################################################\n",
    "for key, value in RunDefinition.parameters.items():\n",
    "    value.plot_in_corner = True\n",
    "    value.corner_label = key.replace(\"_\",\" \")\n",
    "\n",
    "##################################################################\n",
    "# Define axis properties of spectral plot if run_mode == 'evaluate'\n",
    "##################################################################\n",
    "RunDefinition.plot_kwargs[\"spec_xlabel\"] = 'Wavelength [micron]'\n",
    "RunDefinition.plot_kwargs[\"spec_ylabel\"] = r'$(R_{\\rm P}/R_*)^2$ [ppm]'\n",
    "RunDefinition.plot_kwargs[\"y_axis_scaling\"] = 1e6 # so we have units of ppm\n",
    "RunDefinition.plot_kwargs[\"xscale\"] = 'linear'\n",
    "RunDefinition.plot_kwargs[\"yscale\"] = 'linear'\n",
    "\n",
    "# Use at least ~100 samples to plot 3 sigma curves\n",
    "RunDefinition.plot_kwargs[\"nsample\"] = 10\n",
    "\n",
    "##################################################################\n",
    "# Define from which observation object to take P-T\n",
    "# in evaluation mode (if run_mode == 'evaluate'),\n",
    "# add PT-envelope plotting options\n",
    "##################################################################\n",
    "RunDefinition.plot_kwargs[\"take_PTs_from\"] = 'JWST/NIRSPEC/PRISM' \n",
    "RunDefinition.plot_kwargs[\"temp_limits\"] = [150, 3000]\n",
    "RunDefinition.plot_kwargs[\"press_limits\"] = [1e1, 1e-7]\n",
    "\n",
    "# If in doubt, define all of the plot_kwargs used here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running the retrieval\n",
    "\n",
    "At this point, we are ready to run the retrieval! All we need to do is pass the RunDefinition we just created to the Retrieval class and call its run() method. There are a few additional parameters that can be adjusted, but the defaults should work well for almost all use cases. In general it may not be wise to run a retrieval on your laptop, as it can be quite computationally expensive. However, the included HST example should be able to be run fairly easily! (~an hour).\n",
    "\n",
    "**_NOTE:_** This retrieval example only uses 40 live points so that it can be run quickly. However, this should be increased to at least 400 live points for a full retrieval, and will probably require a server or cluster.\n",
    "\n",
    "Once the retrieval is complete, we can use the `plot_all` function to generate plots of the best fit spectrum, the pressure-temperature profile and the corner plots.\n",
    "Most of the various parameters used to control pyMultiNest or Ultranest can be set in the `retrieval.run()` function, see the [API documentation](../../autoapi/petitRADTRANS/retrieval/retrieval/index.html)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "tags": []
   },
   "outputs": [],
   "source": [
    "# If you want to run the retrieval, you need to choose a different output directory name,\n",
    "# due to pRT requirements.\n",
    "output_dir = f\"./\"\n",
    "\n",
    "retrieval = Retrieval(RunDefinition,\n",
    "                      output_dir = output_dir,\n",
    "                      sample_spec = False,         # Output the spectrum from nsample random samples.                     \n",
    "                      pRT_plot_style=True,         # We think that our plots look nice.\n",
    "                      ultranest=False)             # Let's use pyMultiNest rather than Ultranest\n",
    "\n",
    "# Default pymultinest retrieval setup, but with only 40 live points                      \n",
    "retrieval.run(sampling_efficiency=0.8,\n",
    "              const_efficiency_mode=False,\n",
    "              n_live_points=40,\n",
    "              log_z_convergence=0.5,\n",
    "              step_sampler=False,\n",
    "              warmstart_max_tau=0.5,\n",
    "              n_iter_before_update=50,\n",
    "              resume=False,\n",
    "              max_iters=0,\n",
    "              frac_remain=0.1,\n",
    "              importance_nested_sampling = True,\n",
    "              Lepsilon=0.3) \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Automatically generate all of the standard output plots. The contribution \n",
    "# argument means that the PT profile and abundance profile plots will display \n",
    "# the contribution function. The mode argument means we'll be plotting a model\n",
    "# based on the median retrieved parameter values, rather than the minimum likelihood\n",
    "# model.\n",
    "\n",
    "retrieval.plot_all(contribution = True, mode = 'median')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Contact**\n",
    "\n",
    "If you need any additional help, don't hesitate to contact [Evert Nasedkin](mailto:nasedkinevert@gmail.com?subject=[petitRADTRANS]%20Retrievals)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.11"
  },
  "pycharm": {
   "stem_cell": {
    "cell_type": "raw",
    "metadata": {
     "collapsed": false
    },
    "source": []
   }
  },
  "vscode": {
   "interpreter": {
    "hash": "207b56136aaee7ec9eed3aa3bd9bf42bb51cfe74d6567821df16aa3ffeea32bf"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
